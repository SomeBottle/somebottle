### Hi there ヾ(≧▽≦*)o  

![](https://cdn.jsdelivr.net/gh/SomeBottle/somebottle@latest/komi.gif)  

俺是Somebottle，中文昵称些瓶 <del>简写xp</del>  
同时也是动画/Minecraft短片制作爱好者~
大学生一枚（也是单身狗一条），脑袋里经常有些好玩的想法，总想写些有趣的项目.  

我基于自己的想法和咸鱼的技术写出了一些有趣的玩意:  
* [SKLINE](https://github.com/SomeBottle/skline) TUI游戏，贪吃的线  
* [OdIndex](https://github.com/SomeBottle/OdIndex) 咱的onedrive列表程序，麻雀虽小五脏俱全Σ(っ °Д °;)っ~  
* [-O-](https://github.com/SomeBottle/-O-) 静态，利用github api的博客系统  
* [BBlock](https://github.com/SomeBottle/BBlock) 脑袋里突然蹦出来的方块音乐播放器  
and so on...

咱目前也仅仅窥探到了冰山一角，希望有dalao能给予指教(⊙ˍ⊙)，在如今瞬息万变的互联网上可谓是学无止境。  

<img src='https://github-readme-stats.vercel.app/api?username=SomeBottle&show_icons=true&hide_border=true' align='right'></img>

### 主页
博客：[bottle.moe](https://bottle.moe)  
学习仓库：[Cat Note](https://github.com/cat-note/bottleofcat)  
博客园小站：[cnblogs/somebottle](https://www.cnblogs.com/somebottle)  
bilibili：[https://space.bilibili.com/13482355](https://space.bilibili.com/13482355)  
常来这看看，来这看看~🎵  
